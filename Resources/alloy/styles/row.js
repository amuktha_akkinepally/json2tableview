module.exports = [ {
    isApi: true,
    priority: 1000.0005,
    key: "TableViewRow",
    style: {
        layout: "horizontal"
    }
}, {
    isApi: true,
    priority: 1000.0006,
    key: "Label",
    style: {
        color: "#000",
        font: {
            fontFamily: "Ariel",
            fontSize: "20dp",
            fontWeight: "bold"
        }
    }
}, {
    isId: true,
    priority: 100000.0004,
    key: "icon",
    style: {
        left: 0,
        height: 50,
        width: 50
    }
} ];