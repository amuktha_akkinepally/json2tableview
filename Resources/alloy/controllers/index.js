function Controller() {
    function openDetail(e) {
        alert("row index = " + JSON.stringify(e.index));
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "#FFF",
        exitOnClose: true,
        navBarHidden: true,
        title: "Categories",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.table = Ti.UI.createTableView({
        id: "table"
    });
    $.__views.index.add($.__views.table);
    openDetail ? $.__views.table.addEventListener("click", openDetail) : __defers["$.__views.table!click!openDetail"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var data = [];
    var sendit = Ti.Network.createHTTPClient({
        onerror: function(e) {
            Ti.API.debug(e.error);
            alert("There was an error during the connection");
        },
        timeout: 1e3
    });
    sendit.open("GET", "https://maps.googleapis.com/maps/api/place/nearbysearch/json?types=hospital&location=13.01883,80.266113&radius=1000&sensor=false&key=AIzaSyDStAQQtoqnewuLdFwiT-FO0vtkeVx8Sks");
    sendit.send();
    sendit.onload = function() {
        var json = JSON.parse(this.responseText);
        0 == json.length && ($.table.headerTitle = "The database row is empty");
        var countries = json.results;
        for (var i = 0, iLen = countries.length; iLen > i; i++) {
            data.push(Alloy.createController("row", {
                icon: countries[i].icon,
                name: countries[i].name
            }).getView());
            Ti.API.info(countries[i].icon);
            Ti.API.info(countries[i].name);
        }
        $.table.setData(data);
    };
    $.index.open();
    __defers["$.__views.table!click!openDetail"] && $.__views.table.addEventListener("click", openDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;